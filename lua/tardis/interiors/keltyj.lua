-- 1983 TARDIS (NOW WITH CLASSIC DOORS WOW!!!)

local T={}
T.Base="base"
T.Name="1983 TARDIS"
T.ID="keltyj"
T.EnableClassicDoors = true
T.Versions = {
    main = {
        classic_doors_id = "keltyj",
        double_doors_id = "keltyjolddoors",
    },
	other = {
		{
        name = "Master's TARDIS",
        classic_doors_id = "keltyjmaster",
        double_doors_id = "keltyjmasterolddoors",
		},
	},
}
T.Interior={
	Model="models/doctormemes/keltyj/interior.mdl",
	Portal={
		pos=Vector(0,-168,45.5),
		ang=Angle(0,90,0),
		width=68,
		height=90
	},
	Fallback={
		pos=Vector(0,-143.5,3.5),
		ang=Angle(0,90,0),
	},
	Scanners = {
		{
			mat = "models/doctormemes/keltyj/scanner",
			width = 1024,
			height = 1024,
			ang = Angle(0,0,0),
			fov = 60,
		}
	},  
--	Screens={
--		{
--     	   pos=Vector(-126.86431,150.19844,101.23190),
--       	   ang=Angle(0,32.1,90),
--       	   width=1040,
--        	   height=860,
--		   visgui_rows=6
--		}
--	},
	ScreensEnabled= false,
	Sounds={
		Teleport={
			mat="doctormemes/keltyj/keltyjmat.wav"
		},
		Power={
			On="doctormemes/keltyj/bs3.wav",
			Off="doctormemes/keltyj/bs5.wav"
		},
		Door={
			enabled=true,
			open = "doctormemes/keltyj/big doors.wav",
			close = "doctormemes/keltyj/big doors.wav",
		},
	},
	ExitDistance=660, -- no more weed :(
	Sequences="keltyj_sequences",
	Parts={
		door={
			model="models/doctormemes/keltyj/intdoor.mdl",posoffset=Vector(28.3,0,-45),angoffset=Angle(0,-180,0)
		},
		keltyjhatstand=true,
		keltyjconsole={pos=Vector(0,0,0), ang=Angle(0,270,0)},
		keltyjrotor={pos=Vector(0,0,0), ang=Angle(0,270,0)},
		keltyjthrottle={pos=Vector(0,0,0), ang=Angle(0,270,0)},
		keltyjdoorswitch={pos=Vector(0,0,0), ang=Angle(0,270,0)},
		keltyjfloat={pos=Vector(0,0,0), ang=Angle(0,270,0)},
		intdoor = { model = "models/doctormemes/keltyj/bigdoorsclassic.mdl", },
		keltyjflight={pos=Vector(0,0,0), ang=Angle(0,30,0)},
		keltyjdoorlock={pos=Vector(0,0,0), ang=Angle(0,150,0)},
		keltyjwindowswitch={pos=Vector(0,0,0), ang=Angle(0,30,0)},
		keltyjwindow=true,
		keltyjwallsecret=true,
		keltyjsetsecret=true,
		keltyjceiling=true,
		keltyjceiling2=true,
		keltyjbuttons1={pos=Vector(0,0,0), ang=Angle(0,270,0)},
		keltyjbuttons2={pos=Vector(0,0,0), ang=Angle(0,270,0)},
		keltyjbuttons3={pos=Vector(0,0,0), ang=Angle(0,270,0)},
		keltyjbuttons4={pos=Vector(0,0,0), ang=Angle(0,270,0)},
		keltyjbuttons5={pos=Vector(0,0,0), ang=Angle(0,270,0)},
		keltyjbuttons6={pos=Vector(0,0,0), ang=Angle(0,270,0)},
		keltyjdoorframe=true,
--		keltyjscanner=true,
		keltyjroundel=true,
		keltyjpb1={pos=Vector(0,0,0), ang=Angle(0,270,0)},
		keltyjpb2={pos=Vector(0,0,0), ang=Angle(0,270,0)},
		keltyjpb3={pos=Vector(0,0,0), ang=Angle(0,270,0)},
		keltyjpb4={pos=Vector(0,0,0), ang=Angle(0,270,0)},
		keltyjpb5={pos=Vector(0,0,0), ang=Angle(0,270,0)},
		keltyjpb6={pos=Vector(0,0,0), ang=Angle(0,270,0)},
		keltyjpb7={pos=Vector(0,0,0), ang=Angle(0,270,0)},
		keltyjpb8={pos=Vector(0,0,0), ang=Angle(0,270,0)},
		keltyjpb9={pos=Vector(0,0,0), ang=Angle(0,270,0)},
		keltyjscreen1={pos=Vector(0,0,0), ang=Angle(0,270,0)},
		keltyjpb10={pos=Vector(0,0,0), ang=Angle(0,270,0)},
		keltyjpb11={pos=Vector(0,0,0), ang=Angle(0,270,0)},
		keltyjpb12={pos=Vector(0,0,0), ang=Angle(0,270,0)},
		keltyjpb13={pos=Vector(0,0,0), ang=Angle(0,270,0)},
		keltyjpb14={pos=Vector(0,0,0), ang=Angle(0,270,0)},
		keltyjpb15={pos=Vector(0,0,0), ang=Angle(0,270,0)},
		keltyjcorridors1=true,
		keltyjcorridors2=true,
		keltyjsmalldoor={pos=Vector(0.01,0.1,0), ang=Angle(0,0,0)},
		keltyjsmalldoor2={pos=Vector(66.3,403.44,0), ang=Angle(0,-90,0)},
		keltyjsmalldoor3={pos=Vector(-265.38,3.4,0), ang=Angle(0,180,0)},

	},
	IdleSound={
		{
			path="doctormemes/keltyj/interior.wav",
			volume=1	
		}
	},
	LightOverride={
		basebrightness=0.3,
		nopowerbrightness=0.2
	},
	Light={
		color=Color(255,255,255),
		warncolor=Color(255,0,0),
		pos=Vector(0,0,150),
		brightness=2
	},
		Lights = {
		{color = Color(255, 255, 255),		pos = Vector(387,270,150),	brightness = 1,		warncolor = Color(255, 0, 0),},
		{color = Color(255, 255, 255),		pos = Vector(-307,-348,150),	brightness = 1,		warncolor = Color(255, 0, 0),},
	},
	TipSettings={
			style="classic",
            		view_range_max=70,
            		view_range_min=65,
	},
	PartTips={
        keltyjthrottle	=	{pos=Vector(15.842 , -37.9202 , 41.28642 ), text="Demat Switch", right = true, down = true},
        keltyjflight	=	{pos=Vector(24.91884 , 32.67967 , 41.28642 ), text="Flight", right = true},
        keltyjpb2	=	{pos=Vector(32.4896 , -19.3628 , 41.6043 ), text="Coordinates", right = true},
        keltyjpb3	=	{pos=Vector(19.4917 , 24.2515 , 45.2932 ), text="Physical Lock"},
        keltyjpb8	=	{pos=Vector(20.5196 , -23.8827 , 45.4452 ), text="Vortex Flight"},
		keltyjpb13	=	{pos=Vector(23.85, -28.866, 43.377 ), text="Handbrake", down = true, right = true},
		keltyjpb11	=	{pos=Vector(-33.373, -21.951, 41.315 ), text="Manual Destination Selection", down = true, right = true},
		keltyjpb15	=	{pos=Vector(-21.98, 17.525, 45.97 ), text="Redecoration"},
		keltyjpb12	=	{pos=Vector(34.996, 19.207, 41.189 ), text="Engine Release", down = true},
	},
	CustomTips={
		{pos=Vector(11.13780 , -38.31310 , 44.56268 ),    text="Door Switch"},
		{pos=Vector(26.99072 , 28.43926 , 41.28642 ),     text="Float"},
		{pos=Vector(-38.1245 , 9.15501 , 44.56268 ),     text="Scanner Window"},
		{pos=Vector(-40.7608 , 5.24052 , 41.28642 ),     text="Door Lock", right = true},
		{pos=Vector(-26.1554 , 10.3739 , 46.0975 ),     text="Ceiling", right = true},
		{pos=Vector(40.789, 6.665, 41.019 ),     text="Power", down = true},
		{pos=Vector(-0.267, 39.734, 41.144 ),     text="Cloaking Device", down = true, right = true},
		{pos=Vector(-11.8279 , -28.2923 , 45.8252 ),     text="Repair"},
		{pos=Vector(-25.943, 32.541, 41.507 ),     text="H.A.D.S.", down = true},
		{pos=Vector(-25.055 , -3.8484 , 48.1652 ),     text="Fast Return", right = true},
		{pos=Vector(-33.51863 , 3.98609 , 45.12905 ),     text="Security", right = true},
		{pos=Vector(-19.08, 24.391, 45.248 ),     text="Scanner", down = true},
	},
	IntDoorAnimationTime = 2.4,
}

T.Exterior={
	Model="models/doctormemes/keltyj/exterior.mdl",
	Mass=5000,
	DoorAnimationTime=0.6,
	ScannerOffset=Vector(30,0,50),	
	Portal={
		pos=Vector(28.3,0,45),
		ang=Angle(0,0,0),
		width=41,
		height=88
	},
	Fallback={
		pos=Vector(49,0,5),
		ang=Angle(0,0,0)
	},
	Light={
		enabled=true,
		pos=Vector(0,0,113),
		color=Color(255,240,200)
	},
	Sounds={
		Lock="doctormemes/keltyj/lock.wav",
		Door={
			enabled=true,
			open="doctormemes/keltyj/doorext_open.wav",
			close="doctormemes/keltyj/doorext_close.wav"
		},
	},
	Parts={
		door={
			model="models/doctormemes/keltyj/extdoor.mdl",posoffset=Vector(-28.3,0,-45),angoffset=Angle(0,0,0),AnimateSpeed = 6
		},
		vortex={
			model="models/doctormemes/keltyj/vortex.mdl",
			pos=Vector(0,0,50),
			ang=Angle(0,90,0),
			scale=10			
		}
	},
	Teleport={
		SequenceSpeed=0.77,
		SequenceSpeedFast=0.935,
		DematSequence={
			175,
			230,
			100,
			150,
			50,
			100,
			0
		},
		MatSequence={
			100,
			50,
			150,
			100,
			200,
			150,
			255
		}
	}
}

TARDIS:AddInterior(T)