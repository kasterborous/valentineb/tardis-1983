-- 1983 TARDIS (Master Classic Doors)

local T={}
T.Base="keltyj"
T.IsVersionOf = "keltyj"
T.Name="Master's 1983 TARDIS"
T.ID="keltyjmaster"
T.Interior={
	Parts={
		keltyjsmalldoor=false,
		keltyjsmalldoor2=false,
		keltyjsmalldoor3=false,
		keltyjpb4=false,
		keltyjsmalldoorgray={pos=Vector(0.01,0.1,0), ang=Angle(0,0,0)},
		keltyjsmalldoorgray2={pos=Vector(66.3,403.44,0), ang=Angle(0,-90,0)},
		keltyjsmalldoorgray3={pos=Vector(-265.38,3.4,0), ang=Angle(0,180,0)},
		keltyjpb4master={pos=Vector(0,0,0), ang=Angle(0,270,0)},
		keltyjhatstand=false,
	},
}

T.Interior.TextureSets = {
    ["normal"] = {
        prefix = "models/doctormemes/keltyjmaster/",
        { "self", 1, "master wall" }, -- Walls
		{ "self", 4, "master scanner wall" }, 
		{ "keltyjwindow", 0, "master scanner wall" }, 
		{ "self", 12, "master big doors wall" }, 
		{ "self", 12, "master big doors wall" }, 
		{ "self", 13, "master entrance" }, 
		{ "self", 5, "master small door wall" }, 
		{ "keltyjwallsecret", 0, "master wall" }, 
		{ "self", 6, "master gray roundels" }, -- Roundels
		{ "self", 3, "master pillar" }, -- Pillars
		{ "self", 0, "master floor" }, -- Floor
		{ "keltyjceiling", 0, "master ceiling" }, -- Ceiling
		{ "keltyjceiling2", 0, "master ceiling" },
		{ "keltyjsmalldoorgray", 1, "master small door" }, -- Small Door
		{ "keltyjsmalldoorgray", 0, "master gray roundels" },
		{ "keltyjsmalldoorgray2", 1, "master small door" }, -- Small Door 2
		{ "keltyjsmalldoorgray2", 0, "master gray roundels" },
		{ "keltyjsmalldoorgray3", 1, "master small door" }, -- Small Door 3
		{ "keltyjsmalldoorgray3", 0, "master gray roundels" },
		{ "intdoor", 0, "master big doors" }, -- Big Doors
		{ "intdoor", 1, "master gray roundels" },
		{ "intdoor", 2, "master big doors wall" },
		{ "keltyjconsole", 0, "master lower console" }, -- Console
		{ "keltyjconsole", 1, "master middle console" },
		{ "keltyjconsole", 8, "master console panel 1" },
		{ "keltyjconsole", 9, "master console panel 2" },
		{ "keltyjcorridors1", 0, "master wall" }, -- Corridors
		{ "keltyjcorridors1", 6, "master pillar" },
		{ "keltyjcorridors1", 2, "master round pillar" },
		{ "keltyjcorridors1", 4, "master gray roundels" },
		{ "keltyjcorridors1", 3, "master small door wall" },
		{ "keltyjcorridors2", 0, "master wall" },
		{ "keltyjcorridors2", 2, "master round pillar" },
		{ "keltyjcorridors2", 3, "master gray roundels" },
		{ "keltyjcorridors2", 4, "master small door wall" },
    },
}
T.Interior.CustomHooks = {
    master_textures = {
        "Initialize",
        function(self)
            self:ApplyTextureSet("normal")
        end,
    },
}

T.Exterior={}

TARDIS:AddInterior(T)