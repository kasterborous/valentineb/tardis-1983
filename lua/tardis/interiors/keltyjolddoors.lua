-- 1983 TARDIS (With stinky old doors)

local T={}
T.Base="keltyj"
T.IsVersionOf = "keltyj"
T.Name="1983 TARDIS (Doors)"
T.ID="keltyjolddoors"
T.EnableClassicDoors = false
T.Interior={
	Model="models/doctormemes/keltyj/interior.mdl",
	Portal={
		pos=Vector(0,-241.9,44.158),
		ang=Angle(0,90,0),
		width=90,
		height=100
	},
	Fallback={
		pos=Vector(0,-220,3),
		ang=Angle(0,90,0),
	},
	Sounds={
		Door=false,
	},
	Parts={
		door={
			model="models/doctormemes/keltyj/intdoor.mdl",posoffset=Vector(28.3,0,-45),angoffset=Angle(0,-180,0)
		},
		keltyjolddoorsdoorswitch={pos=Vector(0,0,0), ang=Angle(0,270,0)},
		keltyjbigdoors=true,
		keltyjicube=true,
		intdoor=false,
		keltyjdoorswitch=false,
	},
	CustomTips={
		{pos=Vector(11.13780 , -38.31310 , 44.56268 ),    text="Big Doors"},
	},
}

T.Exterior={}

TARDIS:AddInterior(T)