--1983 TARDIS Interior - Control Sequences (advanced mode)

local Seq = {
    ID = "keltyj_sequences",

    ["keltyjpb2"] = {
        Controls = {
            "keltyjpb8",
            "keltyjflight",
            "keltyjpb13",
	    "keltyjthrottle"
        },
        OnFinish = function(self)
		if self.exterior:GetData("vortex") then
            		if IsValid(self) and IsValid(self) then
               			self.exterior:Mat()
            		end
      		end
		if not self.exterior:GetData("vortex") then
		        if IsValid(self) and IsValid(self) then
				self.exterior:Demat()
			end
		end
	end
    },
	["keltyjpb11"] = {
        Controls = {
            "keltyjpb8",
            "keltyjflight",
            "keltyjpb13",
	    "keltyjthrottle"
        },
        OnFinish = function(self)
		if self.exterior:GetData("vortex") then
            		if IsValid(self) and IsValid(self) then
               			self.exterior:Mat()
            		end
      		end
		if not self.exterior:GetData("vortex") then
		        if IsValid(self) and IsValid(self) then
				self.exterior:Demat()
			end
		end
	end
    }
}

TARDIS:AddControlSequence(Seq)