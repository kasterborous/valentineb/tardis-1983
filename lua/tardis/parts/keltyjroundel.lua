local PART={}
PART.ID = "keltyjroundel"
PART.Name = "1983 TARDIS Roundel"
PART.Model = "models/doctormemes/keltyj/roundel.mdl"
PART.AutoSetup = true
PART.ShouldTakeDamage = true

if SERVER then

	function PART:Use(activator,ply)

		local wallsecret = TARDIS:GetPart(self.interior,"keltyjwallsecret")
		local set = TARDIS:GetPart(self.interior,"keltyjsetsecret")

		if ( self:GetOn() ) then
			self:EmitSound(Sound("doctormemes/keltyj/roundel.wav"))
			wallsecret:SetRenderMode( RENDERMODE_NORMAL )
			wallsecret:SetColor (Color(255,255,255,255))
			wallsecret:SetCollide(true,true)
			set:SetRenderMode( RENDERMODE_TRANSALPHA )
			set:SetColor (Color(255,255,255,0))
			set:SetCollide(false,true)
		else
			self:EmitSound(Sound("doctormemes/keltyj/roundel.wav"))
			wallsecret:SetRenderMode( RENDERMODE_TRANSALPHA )
			wallsecret:SetColor (Color(255,255,255,0))
			wallsecret:SetCollide(false,true)
			set:SetRenderMode( RENDERMODE_NORMAL )
			set:SetColor (Color(255,255,255,255))
			set:SetCollide(true,true)
		end
 	end
end

TARDIS:AddPart(PART,e)