local PART={}
PART.ID = "keltyjolddoorsdoorswitch"
PART.Name = "1983 TARDIS Door Switch"
PART.Model = "models/doctormemes/keltyj/doorswitch.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 3
PART.Sound = "doctormemes/keltyj/door switch.wav"
PART.ShouldTakeDamage = true

if SERVER then

	function PART:Use(activator,ply)
		local intdoors = TARDIS:GetPart(self.interior,"keltyjbigdoors")
		local icube = TARDIS:GetPart(self.interior,"keltyjicube")

		if ( self:GetOn() ) then
			intdoors:Toggle( !intdoors:GetOn(), activator )
			icube:SetCollide(false,true)
		else
			intdoors:Toggle( !intdoors:GetOn(), activator )
			icube:SetCollide(true,true)
		end
 	end
end

TARDIS:AddPart(PART,e)