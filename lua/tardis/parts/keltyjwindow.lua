local PART={}
PART.ID = "keltyjwindow"
PART.Name = "1983 TARDIS Scanner Window"
PART.Model = "models/doctormemes/keltyj/window.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 0.34

if SERVER then

	function PART:Use()
		if ( self:GetOn() ) then
			self:EmitSound( Sound( "doctormemes/keltyj/window.wav" ))
		else
			self:EmitSound( Sound( "doctormemes/keltyj/window.wav" ))
		end
	end

	function PART:Toggle( bEnable, ply )
		if ( bEnable ) then
			self:SetOn( true )
			self:EmitSound( Sound( "doctormemes/keltyj/window.wav" ))
		else
			self:SetOn( false )
			self:EmitSound( Sound( "doctormemes/keltyj/window.wav" ))
		end
	end
end

TARDIS:AddPart(PART,e)