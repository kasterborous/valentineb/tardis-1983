local PART={}
PART.ID = "keltyjsmalldoorgray"
PART.Name = "Master's 1983 TARDIS Small Door"
PART.Model = "models/doctormemes/keltyj/smalldoorgray.mdl"
PART.AutoSetup = true
PART.ShouldTakeDamage = true
PART.Collision = true
PART.SoundOn = "doctormemes/keltyj/lqdoorsopen.wav"
PART.SoundOff = "doctormemes/keltyj/lqdoorsclose.wav"
PART.Animate = true
PART.AnimateSpeed = 1.3

if SERVER then

	function PART:Use()
		if ( self:GetOn() ) then
			self:SetCollide(true,false)
		else
			self:SetCollide(false,false)
		end
	end
end

TARDIS:AddPart(PART,e)