local PART={}
PART.ID = "keltyjpb1"
PART.Name = "1983 TARDIS Pressable Buttons 1"
PART.Model = "models/doctormemes/keltyj/pb1.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.ShouldTakeDamage = true

if SERVER then
	function PART:Use(activator)
		local ceiling=self.interior:GetPart("keltyjceiling")
		if ( self:GetOn() ) then
			self:EmitSound(Sound("doctormemes/keltyj/toggle.wav"))
			self:EmitSound(Sound("doctormemes/keltyj/roundel.wav"))
			ceiling:SetRenderMode( RENDERMODE_NORMAL )
			ceiling:SetColor (Color(255,255,255,255))
		else
			self:EmitSound(Sound("doctormemes/keltyj/toggle.wav"))
			self:EmitSound(Sound("doctormemes/keltyj/roundel.wav"))
			ceiling:SetRenderMode( RENDERMODE_TRANSALPHA )
			ceiling:SetColor (Color(255,255,255,0))
		end
	end
end


TARDIS:AddPart(PART,e)