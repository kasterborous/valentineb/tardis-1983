local PART={}
PART.ID = "keltyjdoorswitch"
PART.Name = "1983 TARDIS Door Switch"
PART.Model = "models/doctormemes/keltyj/doorswitch.mdl"
PART.AutoSetup = true
PART.Animate = true
PART.AnimateSpeed = 3
PART.Sound = "doctormemes/keltyj/door switch.wav"
PART.ShouldTakeDamage = true
PART.Control = "door"

TARDIS:AddPart(PART,e)