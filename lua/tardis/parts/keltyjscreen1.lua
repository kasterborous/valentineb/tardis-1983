local PART={}
PART.ID = "keltyjscreen1"
PART.Name = "1983 TARDIS Screen 1"
PART.Model = "models/doctormemes/keltyj/screen1.mdl"
PART.AutoSetup = true

if SERVER then
        function PART:Think()

		local exterior=self.exterior
		local interior=self.interior
		local power=self.exterior:GetData("power-state")

		if exterior:GetData("flight") or exterior:GetData("teleport") or exterior:GetData("vortex") then
			self:SetSubMaterial(0 , "models/doctormemes/keltyj/Screen 1 Animated")
		else
			self:SetSubMaterial(0 , "models/doctormemes/keltyj/Screen 1")
		end

		if power == false then
			self:SetColor (Color(0,0,0))
		else
			self:SetColor (Color(255,255,255))
		end
        end
end

TARDIS:AddPart(PART,e)