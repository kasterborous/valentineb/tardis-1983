local PART={}
PART.ID = "keltyjthrottle"
PART.Name = "1983 TARDIS Throttle"
PART.Model = "models/doctormemes/keltyj/puck1.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 3
PART.Sound = "doctormemes/keltyj/door switch.wav"
PART.ShouldTakeDamage = true
PART.Control = "teleport"

if SERVER then
	function PART:Use(ply)
		if self.exterior:GetData("teleport") == true or self.exterior:GetData("vortex") == true
		or not self.interior:GetSequencesEnabled()
		then
		TARDIS:Control("teleport", ply)
		else
		TARDIS:ErrorMessage(ply, "Control Sequences are enabled. You must use the sequence.")
		end
	end
end

TARDIS:AddPart(PART,e)

