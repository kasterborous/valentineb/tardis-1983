local PART={}
PART.ID = "keltyjfloat"
PART.Name = "1983 TARDIS Float Lever"
PART.Model = "models/doctormemes/keltyj/puck2.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 3
PART.Sound = "doctormemes/keltyj/door switch.wav"
PART.Control = "float"
PART.ShouldTakeDamage = true

TARDIS:AddPart(PART)