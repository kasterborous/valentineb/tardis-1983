local PART={}
PART.ID = "keltyjsmalldoor2"
PART.Name = "1983 TARDIS Small Door 2"
PART.Model = "models/doctormemes/keltyj/smalldoor.mdl"
PART.AutoSetup = true
PART.ShouldTakeDamage = true
PART.Collision = true
PART.SoundOn = "doctormemes/keltyj/lqdoorsopen.wav"
PART.SoundOff = "doctormemes/keltyj/lqdoorsclose.wav"
PART.Animate = true
PART.AnimateSpeed = 1.3

if SERVER then

	function PART:Use()
		if ( self:GetOn() ) then
			self:SetCollide(true,false)
		else
			self:SetCollide(false,false)
		end
	end
end

TARDIS:AddPart(PART,e)