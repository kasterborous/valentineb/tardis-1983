local PART={}
PART.ID = "keltyjbigdoors"
PART.Name = "1983 TARDIS Big Doors"
PART.Model = "models/doctormemes/keltyj/bigdoors.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 0.4
PART.ShouldTakeDamage = true

if SERVER then
	function PART:Toggle( bEnable, ply )
		if ( bEnable ) then
			self:SetOn( true )
			self:EmitSound( Sound( "doctormemes/keltyj/big doors.wav" ))
		else
			self:SetOn( false )
			self:EmitSound( Sound( "doctormemes/keltyj/big doors.wav" ))
		end
	end
end

TARDIS:AddPart(PART,e)