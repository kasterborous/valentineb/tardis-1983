local PART={}
PART.ID = "keltyjconsole"
PART.Name = "1983 TARDIS Console"
PART.Model = "models/doctormemes/keltyj/console.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.ShouldTakeDamage = true
PART.BypassIsomorphic = true
PART.Control = "thirdperson_careful"

TARDIS:AddPart(PART,e)