local PART={}
PART.ID = "keltyjdoorframe"
PART.Name = "1983 TARDIS Door Frame"
PART.Model = "models/doctormemes/keltyj/doorframe.mdl"
PART.AutoSetup = true
PART.ShouldTakeDamage = true

if SERVER then
	hook.Add("SkinChanged", "keltyj-doorframe", function(ent,i)
		if ent.TardisExterior then
			local keltyjdoorframe=ent:GetPart("keltyjdoorframe")
			if IsValid(keltyjdoorframe) then
				keltyjdoorframe:SetSkin(i)
			end
			if IsValid(ent.interior) then
				local keltyjdoorframe=ent.interior:GetPart("keltyjdoorframe")
				if IsValid(keltyjdoorframe) then
					keltyjdoorframe:SetSkin(i)
				end
			end
		end
	end)
end

TARDIS:AddPart(PART)