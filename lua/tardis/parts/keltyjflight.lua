local PART={}
PART.ID = "keltyjflight"
PART.Name = "1983 TARDIS Flight Lever"
PART.Model = "models/doctormemes/keltyj/puck1.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 3
PART.Sound = "doctormemes/keltyj/door switch.wav"
PART.Control = "flight"
PART.ShouldTakeDamage = true

TARDIS:AddPart(PART)