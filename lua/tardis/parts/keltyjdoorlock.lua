local PART={}
PART.ID = "keltyjdoorlock"
PART.Name = "1983 TARDIS Exterior Doors Lock"
PART.Model = "models/doctormemes/keltyj/puck1.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 3
PART.Sound = "doctormemes/keltyj/door switch.wav"
PART.Control = "doorlock"
PART.ShouldTakeDamage = true

TARDIS:AddPart(PART,e)