local PART={}
PART.ID = "keltyjpb4"
PART.Name = "1983 TARDIS Pressable Buttons 4"
PART.Model = "models/doctormemes/keltyj/pb4.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Sound = "doctormemes/keltyj/toggle.wav"
PART.Control = "power"
PART.ShouldTakeDamage = true


if SERVER then

	function PART:Think(ply)
	local power=self.exterior:GetData("power-state")
	local interior=self.interior
	local console=interior:GetPart("keltyjconsole")
	local rotor=interior:GetPart("keltyjrotor")
	local roundel=interior:GetPart("keltyjroundel")
	local wall=interior:GetPart("keltyjwallsecret")
	local exterior=self.exterior
	local warning=self.exterior:GetData("health-warning")
	local smalldoor=interior:GetPart("keltyjsmalldoor")
	local smalldoor2=interior:GetPart("keltyjsmalldoor2")
	local smalldoor3=interior:GetPart("keltyjsmalldoor3")
	local corridors1=interior:GetPart("keltyjcorridors1")
	local corridors2=interior:GetPart("keltyjcorridors2")
	
		if	power == false then
				console:SetSubMaterial(7 , "models/doctormemes/keltyj/black")
				console:SetSubMaterial(6 , "models/doctormemes/keltyj/black")
				console:SetSubMaterial(2 , "models/doctormemes/keltyj/pentagon off")
				console:SetSubMaterial(14 , "models/doctormemes/keltyj/black")
				console:SetSubMaterial(3 , "models/doctormemes/keltyj/Yellow Lamp Dark")
				console:SetSubMaterial(10 , "models/doctormemes/keltyj/black")
				rotor:SetSubMaterial(3 , "models/doctormemes/keltyj/Glass noenv")
				interior:SetSubMaterial(2 , "models/doctormemes/keltyj/Roundels Dark")
				roundel:SetSubMaterial(0 , "models/doctormemes/keltyj/Roundels Dark")
				wall:SetSubMaterial(1 , "models/doctormemes/keltyj/Roundels Dark")
				interior:SetSubMaterial(11 , "models/doctormemes/keltyj/Studio Light Off")
				smalldoor:SetSubMaterial(0 , "models/doctormemes/keltyj/Roundels Dark")
				smalldoor2:SetSubMaterial(0 , "models/doctormemes/keltyj/Roundels Dark")
				smalldoor3:SetSubMaterial(0 , "models/doctormemes/keltyj/Roundels Dark")
				corridors1:SetSubMaterial(1 , "models/doctormemes/keltyj/Roundels Dark")
				corridors2:SetSubMaterial(1 , "models/doctormemes/keltyj/Roundels Dark")

		else

			if warning == true then
				interior:SetSubMaterial(11 , "models/doctormemes/keltyj/Studio Light Warn")
			else
				interior:SetSubMaterial(11 , "models/doctormemes/keltyj/Studio Light")
			end

				console:SetSubMaterial(7 , "models/doctormemes/keltyj/voltmeter")
				console:SetSubMaterial(6 , "models/doctormemes/keltyj/lights")
				console:SetSubMaterial(2 , "models/doctormemes/keltyj/pentagon")
				console:SetSubMaterial(14 , "models/doctormemes/keltyj/Screen 3")
				console:SetSubMaterial(3 , "models/doctormemes/keltyj/Yellow Lamp")
				console:SetSubMaterial(10 , "models/doctormemes/keltyj/Screen 2	")
				rotor:SetSubMaterial(3 , "models/doctormemes/keltyj/Glass")
				interior:SetSubMaterial(2 , "models/doctormemes/keltyj/Roundels")
				roundel:SetSubMaterial(0 , "models/doctormemes/keltyj/Roundels")
				wall:SetSubMaterial(1 , "models/doctormemes/keltyj/Roundels")
				smalldoor:SetSubMaterial(0 , "models/doctormemes/keltyj/Roundels")
				smalldoor2:SetSubMaterial(0 , "models/doctormemes/keltyj/Roundels")
				smalldoor3:SetSubMaterial(0 , "models/doctormemes/keltyj/Roundels")
				corridors1:SetSubMaterial(1 , "models/doctormemes/keltyj/Roundels")
				corridors2:SetSubMaterial(1 , "models/doctormemes/keltyj/Roundels")

		end
	end
end

TARDIS:AddPart(PART,e)

