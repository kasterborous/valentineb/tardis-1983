local PART={}
PART.ID = "keltyjwindowswitch"
PART.Name = "1983 TARDIS Window Switch"
PART.Model = "models/doctormemes/keltyj/puck2.mdl"
PART.AutoSetup = true
PART.Animate = true
PART.AnimateSpeed = 3
PART.ShouldTakeDamage = true

if SERVER then
	function PART:Use(activator)
		local scannerwindow = TARDIS:GetPart(self.interior,"keltyjwindow")
		self:EmitSound( Sound( "doctormemes/keltyj/door switch.wav" ))
		scannerwindow:Toggle( !scannerwindow:GetOn(), activator )
	end
end

TARDIS:AddPart(PART,e)